include:
  - local: ci/scripts/git-config.yml

# Deploy dist files
# variables
#   - REPO_DIR: directory to deploy
.dist-deploy:
  stage: deploy
  image: gitlab-registry.cern.ch/mro/common/tools/builder/ci-artifacts
  tags: [ k8s-eos ]
  resource_group: deploy
  variables:
    REPO_DIR: 'empty'
    CI_OUTPUT_DIR: "public"
    RSYNC_OPTS: "-v"
  rules:
    - if: $CI_PIPELINE_SOURCE != "pipeline"
    - when: manual
      allow_failure: true
  script:
    - export EOS_PATH="/eos/project/n/ntofci/www/distfiles/$REPO_DIR/"
    - echo "$NTOFCI_PASSWORD" | kinit "$NTOFCI_USERNAME@CERN.CH"
    - mkdir -p "${EOS_PATH}"
    - rsync ${RSYNC_OPTS} ${CI_OUTPUT_DIR}/* ${EOS_PATH}

.dist-rhel-test:
  stage: test
  variables:
    REPO_DIR: 'cc7'
    REPO_FILE: 'MRO-Common.repo'
    TEST_SCRIPT: '../ci-test.sh'
  rules:
    - if: $CI_PIPELINE_SOURCE != "pipeline"
      changes: [ '$REPO_DIR/**' ]
    - when: manual
      allow_failure: true
  script:
    # Install dep repos if needed
    - echo "${REPOSITORIES}" | xargs -r -n 1 yum-config-manager --add-repo

    - yum install -y createrepo
    - cd $REPO_DIR && createrepo .
    - sed -i "s#baseurl=.*#baseurl=file://$(pwd)#" "./$REPO_FILE"
    - yum-config-manager --add-repo "./$REPO_FILE"
    - '"${TEST_SCRIPT}" .'


# backward compatibilty
.dist-cc7-test:
  extends: .dist-rhel-test

.dist-rhel-deploy:
  stage: deploy
  extends: .dist-deploy
  resource_group: deploy
  variables:
    REPO_DIR: 'al9'
  rules:
    - if: '$CI_PIPELINE_SOURCE != "pipeline" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes: [ '$REPO_DIR/**' ]
    - when: manual
      allow_failure: true
  script:
    - rm -rf public && mkdir public && cd public
    - yum install -y createrepo rsync
    - rsync -aiP "../al9/" ./
    - createrepo .
    - cd ..
    - !reference [.dist-deploy, script]

# backward compatibility
.dist-cc7-deploy:
  extends: .dist-rhel-deploy

.dist-deb-build:
  stage: build
  image: debian:stretch-slim
  variables:
    REPO_DIR: 'mro-main'
    CONFIG: '.mini-dinstall.conf'
  rules:
    - if: '$CI_PIPELINE_SOURCE != "pipeline"'
      changes: [ 'debian/**/*' ]
  script:
    # retrieve existing files
    - rm -rf public && mkdir public && cd public

    # Install required tools
    - apt-get update
    - apt-get install -y mini-dinstall rsync
    # Override with updates
    - mkdir -p "$REPO_DIR"
    - rsync -aiP ../debian/stretch/ "./$REPO_DIR/"

    # Update repo info
    - mini-dinstall -c "./$REPO_DIR/$CONFIG" -b
    - rm -rf "./$REPO_DIR/$CONFIG" mini-dinstall
  artifacts:
    paths:
      - public

.dist-deb-deploy:
  extends: .dist-deploy
  variables:
    REPO_DIR: 'debian/stretch'
  rules:
    - if: '$CI_PIPELINE_SOURCE != "pipeline" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes: [ 'debian/**/*' ]
    - when: manual
      allow_failure: true

.dist-js-build:
  stage: build
  image: debian:stretch-slim
  rules:
    - if: '$CI_PIPELINE_SOURCE != "pipeline"'
      changes: [ 'js/**/*' ]
  script:
    # retrieve existing files
    - rm -rf public && mkdir public && cd public

    # Install required tools
    - apt-get update
    - apt-get install -y rsync

    # Override with updates
    - rsync -aiP ../js/ .
  artifacts:
    paths:
      - public

.dist-js-deploy:
  extends: .dist-deploy
  variables:
    REPO_DIR: 'js'
  rules:
    - if: '$CI_PIPELINE_SOURCE != "pipeline" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes: [ 'js/**/*' ]
    - when: manual
      allow_failure: true

.dist-inject:
  stage: deploy
  image: gitlab-registry.cern.ch/linuxsupport/alma9-base:latest
  resource_group: inject
  rules:
    - if: $CI_PIPELINE_SOURCE == "pipeline" && $ARTIFACT_URL
  script:
    - yum install -y unzip git

    - rm -rf downloads && mkdir downloads
    - 'curl -f -L --header "JOB-TOKEN: $CI_JOB_TOKEN" "$ARTIFACT_URL" -o downloads/artifacts.zip'
    - unzip -d downloads downloads/artifacts.zip
    - 'find downloads -name "*el9*.rpm" -exec mv \{\} al9 \;'
    - 'find downloads -name "*.deb" -exec mv \{\} debian/stretch \;'

    - !reference [.git-config, script]

    - '[ ! -d al9 ] || git add al9'
    - '[ ! -d debian ] || git add debian'
    - git pull origin master
    - git commit -m 'Automatic commit'

    # For some reasons gitlab uses a weird internal URL, CI_REPOSITORY_URL is also corrupted (ex:
    - git push origin HEAD:master

.dist-js-inject:
  stage: deploy
  image: gitlab-registry.cern.ch/linuxsupport/alma9-base:latest
  resource_group: inject
  rules:
    - if: $CI_PIPELINE_SOURCE == "pipeline" && $ARTIFACT_URL
  script:
    - yum install -y unzip git

    - rm -rf downloads && mkdir downloads
    - 'curl -f -L --header "JOB-TOKEN: $CI_JOB_TOKEN" "$ARTIFACT_URL" -o downloads/artifacts.zip'
    - unzip -d downloads downloads/artifacts.zip
    - 'find downloads \( -name "*.js" -o -name "*.js.map" \) -exec mv \{\} js \;'

    - !reference [.git-config, script]

    - '[ ! -d js ] || git add js'
    - git pull origin master
    - git commit -m 'Automatic commit'

    # For some reasons gitlab uses a weird internal URL, CI_REPOSITORY_URL is also corrupted (ex:
    - git push origin HEAD:master

#.dist-cc7-inject:
  #extends: .dist-inject
